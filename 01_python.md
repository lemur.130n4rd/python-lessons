******

## Python is a multi-purpose language :

- Data Analysis

- Artificial Intelligence / Machine Learning

- Automation

- Web , Mobile , Desktop apps

- Testing

- Hacking

******

## Python features :

+ - Python executes the code **line by line**
  - Python is a **case sensitive** language
  - Python is a **High-Level** language
  - Python is a **cross-Platform** language
  - Python have a **Huge Comunity**
  - Python have a **large ecosystem**

******

## Python is the world most known programing language among :

- Software Engineers
- Mathematicians
- Data Analysts
- Scientists
- Accountants
- Network Engineers

******

## With python we can solve complex problems in less time with fewer lines of code

Example (if we want to substract the first 3 words in something)

```csharp
str.Substring(0, 3)
// c language 
```

```java
str.substr(0, 3)
// java language
```

```python
str[0:3] 
# python language
```

******

# Using an IDE:

The name IDE comes from Integrated Developement Environment

An IDE is like a text editor with more features

## The features:

- linting
- autocompletion
- unit testing
- debugging
- code Formatting
- code snippets

******

END
