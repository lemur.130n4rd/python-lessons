******

# Python Data types

### <u>VARIABLES</u>

- **Integer** = a real number (<u>10</u>)

- **Float** = a decimal point number (<u>0.22132</u>)

- **String** = a word / character / sentece ("home")

- **Boolean** = a true / false value (<u>True</u>)

- - for **falsy** values we also have can also have
  
  - - **an empty string**
    
    - **0**
    
    - **None**

- **Complex numbers** = an imaginar value (1 + 2j )

- **list** = a collection of values ( ["home", 1 , True] )

- **Tuple** = a list where you can't change the value (1, "Hoe", True)

- **Dictyonary** = a collection of variables

- **Iterables** = objects that can be used in loops (range() , strings , lists, tuples)

******

# Operators in python

| <mark>+ </mark> | <mark>- </mark>  | <mark>*</mark>       | <mark>/</mark> | <mark>//</mark>  | <mark>%</mark>                |
|:---------------:|:----------------:|:--------------------:|:--------------:| ---------------- | ----------------------------- |
| sum             | minus            | multiply             | division       | rounded division | remainer of a division        |
|                 |                  |                      |                |                  |                               |
| <mark>=</mark>  | <mark>==</mark>  | <mark>!=</mark>      | <mark>></mark> | <mark><</mark>   | <mark>>=  <=</mark>           |
| equals          | equals(checking) | not equals(checking) | bigger         | less             | bigger or equal less or equal |

******

# Important Rules

### All the names needs to be meaningful and described to understand better the code

---

### when using x, y, z is often used to define a location

### (x y z value)

---

### We can have variables like in the example but each one can store a different value

- student
- Student
- STUDENT
- student_1

---

END
